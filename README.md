
# k8s-up-local-cluster
Vamos a instalar un cluster local kubernetes, con base en el repositorio kubernetes-the-hard-way https://github.com/kelseyhightower/kubernetes-the-hard-way

Sistema Operativo - Host
===
 Linux, en mi caso uso Arch Linux con DE kde, puedes usar el de tu preferencia.

| Software instalado |
| :--- |
| VirtualBox |
| LibVirt |
| ssh |
| sshpass |
| dig |
| wget |
| curl |
| cfssl & cfssljson https://github.com/cloudflare/cfssl/releases | 
| kubectl |
| helm |


Clonar el siguiente repositorio que contiene los OVAs base, usaremos ubuntu-base-*
https://github.com/jarley91/vbox-ovas.git

Si prefieres puedes descargar el ISO de Ubuntu Server e instalar en una vm en VirtualBox para exportar a OCI, asegúrate que tu vm tenga un adaptador puente únicamente, acceso ssh con el usuario root y en el archivo yaml de netplan la siguiente estructura con los datos de tus subnets:

/etc/netplan/00-installer-config.yaml

```yaml
# This is the network config written by 'subiquity'
network:
  version: 2
  ethernets:
    enp0s3:
      dhcp4: false
      addresses: [192.168.31.250/24]
      gateway4: 192.168.31.1
      nameservers:
        addresses: [8.8.8.8]
```

Configurar archivo .env
===
Configura los valores en el archivo .env dentro de la carpeta scripts, los datos de IP deben ser según tu subnet:

![env, .env](images/env.png)

Servidor DNS
===
Para este servidor usé Debian, puedes elegir la distro Linux que prefieras y configurar tu servidor DNS:

Edita los archivos dentro de la carpeta dns según tu subnet:

![folder-dns, folder-dns](images/folder-dns.png)

```sh
cd scripts
sh create-dns-server.sh
```

Configura en tu host la IP de tu servidore DNS en el archivo /etc/resolv.conf y valida la resolución de nombres:

```sh
cd scripts/
source .env
dig controller-1.${DOMAIN_NAME} +short
```

![test-dns, Test DNS](images/test-dns.png)

Aprovisiona tus VMs
===
```sh
sh create-servers.sh
```

* [Provisioning Compute Resources](https://github.com/kelseyhightower/kubernetes-the-hard-way/blob/master/docs/03-compute-resources.md)

Crea los archivos TLS
===
```sh
sh create-ca-tls-certificates.sh
```

* [Provisioning the CA and Generating TLS Certificates](https://github.com/kelseyhightower/kubernetes-the-hard-way/blob/master/docs/04-certificate-authority.md)

Crea los archivos de autenticación
===
```sh
sh create-config-auth.sh
```

* [Generating Kubernetes Configuration Files for Authentication](https://github.com/kelseyhightower/kubernetes-the-hard-way/blob/master/docs/05-kubernetes-configuration-files.md)

Crea el archivo para el cifrado de secretos
===
```sh
sh create-encrypt-conf.sh
```

* [Generating the Data Encryption Config and Key](https://github.com/kelseyhightower/kubernetes-the-hard-way/blob/master/docs/06-data-encryption-keys.md)

Inicia etcd
===
```sh
sh exec-boot-etcd.sh
```

* [Bootstrapping the etcd Cluster](https://github.com/kelseyhightower/kubernetes-the-hard-way/blob/master/docs/07-bootstrapping-etcd.md)

Inicia el control plane
===
```sh
sh exec-boot-control-plane.sh
```

* [Bootstrapping the Kubernetes Control Plane](https://github.com/kelseyhightower/kubernetes-the-hard-way/blob/master/docs/08-bootstrapping-kubernetes-controllers.md)

Inicia los workers
===
```sh
sh exec-boot-workers-nodes.sh
```

* [Bootstrapping the Kubernetes Worker Nodes](https://github.com/kelseyhightower/kubernetes-the-hard-way/blob/master/docs/09-bootstrapping-kubernetes-workers.md)

Habilita el acceso remoto a tu cluster
===
```sh
sh config-remote-access.sh
```

* [Configuring kubectl for Remote Access](https://github.com/kelseyhightower/kubernetes-the-hard-way/blob/master/docs/10-configuring-kubectl.md)

![remote-acces, remote acces](images/remote-acces.png)

Crea los routes para la comunicación entre los pods
===
```sh
sh add-routes-pods-workers-nodes.sh
```

* [Provisioning Pod Network Routes](https://github.com/kelseyhightower/kubernetes-the-hard-way/blob/master/docs/11-pod-network-routes.md)

Registra a tus cotrollers
===
```sh
sh exec-register-control-plane.sh
```

![register-controllers, enregister controllersv](images/register-controllers.png)

Agrega labels y taints a los controlles y workers
===
```sh
sh set-labels-taints.sh
```

![labels-taints, labels taints](images/labels-taints.png)

Instala coredns, metallb, metrics-server, kubernetes-dashboard, ingress-nginx, prometheus y grafana
===
```sh
sh install-addons.sh
```

CoreDNS
===

```sh
kubectl run busybox --image=busybox:1.28 --command -- sleep 3600
kubectl exec -ti busybox -- nslookup kubernetes
```
![coredns, coredns](images/coredns.png)

MetalLB
===
![metallb-1, metallb 1](images/metallb-1.png)
![metallb-2, metallb 2](images/metallb-2.png)

Metric Server
===
![metric-server, metric server](images/metric-server.png)

kubernetes Dashboard
===
![kubernetes-dashboard, kubernetes dashboard](images/kubernetes-dashboard.png)

ingress-nginx, prometheus y grafana
===
![nginx, nginx](images/nginx.png)
![grafana, grafana](images/grafana.png)