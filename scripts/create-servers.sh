source ./.env

NAME_VM=${PREFIX_NAME_CLUSTER_SERVER}-balancer
VBoxManage import ${PATH_OVA_FILE_SERVER_K8S} --vsys 0 --eula accept --vmname ${NAME_VM} --memory 1024 --cpus ${VM_CPUS} --options keepallmacs
VBoxManage modifyvm ${NAME_VM} --nic1 bridged --nictype1 82540EM --bridgeadapter1 ${HOST_NI_FOR_BRIDGE}
VBoxManage modifyvm ${NAME_VM} --nic2 hostonly --hostonlyadapter2 ${VBOX_NI_FOR_HOST_ONLY}

VBoxManage startvm ${NAME_VM} --type headless
sleep 30s

IP_EXT=$(dig ${API_SERVER_EXTERNAL_NAME} +short)
IP_INT=$(dig ${API_SERVER_INTENAL_NAME} +short)
sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "sed -i 's/${IP_TEMP}/${IP_EXT}/g' ${PATH_NETPLAN_CONFIG}"
sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "sed -i 's/${IP_DNS_TEMP}/${IP_DNS_SERVER}/g' ${PATH_NETPLAN_CONFIG}"
sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "echo '    enp0s8:' >> ${PATH_NETPLAN_CONFIG}"
sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "echo '      dhcp4: false' >> ${PATH_NETPLAN_CONFIG}"
sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "echo '      addresses: [${IP_INT}/24]' >> ${PATH_NETPLAN_CONFIG}"
sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "apt install -y nginx net-tools resolvconf && systemctl enable nginx && mkdir -p /etc/nginx/tcpconf.d"
sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "echo 'nameserver ${IP_DNS_SERVER}' >> /etc/resolvconf/resolv.conf.d/head"
sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "systemctl enable --now resolvconf && resolvconf -u"
sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "sed -i '5i\include /etc/nginx/tcpconf.d/*;\' /etc/nginx/nginx.conf"
sshpass -f ../utils/password scp -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" ../balancer/k8s-balancer.conf root@${IP_TEMP}:~/

IP_INT_CONTROLLER1=$(dig controller-1.int.${DOMAIN_NAME} +short)
sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "sed -i 's/ip_int_controller1/${IP_INT_CONTROLLER1}/g' ~/k8s-balancer.conf"

IP_INT_CONTROLLER2=$(dig controller-2.int.${DOMAIN_NAME} +short)
sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "sed -i 's/ip_int_controller2/${IP_INT_CONTROLLER2}/g' ~/k8s-balancer.conf"

IP_INT_CONTROLLER3=$(dig controller-3.int.${DOMAIN_NAME} +short)
sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "sed -i 's/ip_int_controller3/${IP_INT_CONTROLLER3}/g' ~/k8s-balancer.conf"

sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "mv ~/k8s-balancer.conf /etc/nginx/tcpconf.d/ && nginx -s reload"
sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "hostnamectl set-hostname balancer.${DOMAIN_NAME}"
sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "netplan apply"

for instance in controller-1 controller-2 controller-3 lb-infra-1 lb-infra-2 worker-1 worker-2 worker-3; do
  NAME_VM=${PREFIX_NAME_CLUSTER_SERVER}-${instance}
  VBoxManage import ${PATH_OVA_FILE_SERVER_K8S} --vsys 0 --eula accept --vmname ${NAME_VM} --memory ${VM_MEMORY} --cpus ${VM_CPUS} --options keepallmacs
  VBoxManage modifyvm ${NAME_VM} --nic1 bridged --nictype1 82540EM --bridgeadapter1 ${HOST_NI_FOR_BRIDGE}
  VBoxManage modifyvm ${NAME_VM} --nic2 hostonly --hostonlyadapter2 ${VBOX_NI_FOR_HOST_ONLY}

  VBoxManage startvm ${NAME_VM} --type headless
  sleep 30s

  IP_EXT=$(dig ${instance}.${DOMAIN_NAME} +short)
  IP_INT=$(dig ${instance}.int.${DOMAIN_NAME} +short)
  SWAP_FSTAB="/swap.img"
  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "sed -i 's/${SWAP_FSTAB//\//\\/}/#${SWAP_FSTAB//\//\\/}/g' /etc/fstab"
  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "swapoff -a"
  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "sed -i 's/${IP_TEMP}/${IP_EXT}/g' ${PATH_NETPLAN_CONFIG}"
  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "sed -i 's/${IP_DNS_TEMP}/${IP_DNS_SERVER}/g' ${PATH_NETPLAN_CONFIG}"
  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "echo '    enp0s8:' >> ${PATH_NETPLAN_CONFIG}"
  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "echo '      dhcp4: false' >> ${PATH_NETPLAN_CONFIG}"
  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "echo '      addresses: [${IP_INT}/24]' >> ${PATH_NETPLAN_CONFIG}"
  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "apt install -y net-tools resolvconf"
  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "hostnamectl set-hostname ${instance}.${DOMAIN_NAME}"
  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "echo 'nameserver ${IP_DNS_SERVER}' >> /etc/resolvconf/resolv.conf.d/head"
  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${IP_TEMP} "systemctl enable --now resolvconf && resolvconf -u && netplan apply"
done