source ./.env

for instance in controller-1 controller-2 controller-3; do
  sshpass -f ../utils/password scp -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" .env boot-etcd.sh root@${instance}.${DOMAIN_NAME}:~/
  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${instance}.${DOMAIN_NAME} "sh boot-etcd.sh"
  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${instance}.${DOMAIN_NAME} "rm .env boot-etcd.sh"
done