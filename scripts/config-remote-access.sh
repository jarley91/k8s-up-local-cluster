source ./.env

# The Admin Kubernetes Configuration File
{
  kubectl config set-cluster ${DOMAIN_NAME} \
    --certificate-authority=${K8S_WORK_DIR}/tls/ca.pem \
    --embed-certs=true \
    --server=https://${API_SERVER_EXTERNAL_NAME}:6443

  kubectl config set-credentials admin \
    --client-certificate=${K8S_WORK_DIR}/tls/admin.pem \
    --client-key=${K8S_WORK_DIR}/tls/admin-key.pem

  kubectl config set-context ${DOMAIN_NAME} \
    --cluster=${DOMAIN_NAME} \
    --user=admin

  kubectl config use-context ${DOMAIN_NAME}
}
