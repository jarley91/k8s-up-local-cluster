source ./.env

# The Encryption Key
ENCRYPTION_KEY=$(head -c 32 /dev/urandom | base64)

# The Encryption Config File
cat > ${K8S_WORK_DIR}/encryption-config.yaml <<EOF
kind: EncryptionConfig
apiVersion: v1
resources:
  - resources:
      - secrets
    providers:
      - aescbc:
          keys:
            - name: key1
              secret: ${ENCRYPTION_KEY}
      - identity: {}
EOF

for instance in controller-1 controller-2 controller-3; do
  sshpass -f ../utils/password scp -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" ${K8S_WORK_DIR}/encryption-config.yaml root@${instance}.${DOMAIN_NAME}:~/
done
