source ./.env

CLUSTER_INTERNAL_IP=$(dig ${API_SERVER_INTENAL_NAME} +short)
CUR_DIR=$(pwd)
cd ${K8S_WORK_DIR}/tls

# The kubelet Kubernetes Configuration File
for instance in lb-infra-1 lb-infra-2 worker-1 worker-2 worker-3; do
  kubectl config set-cluster ${DOMAIN_NAME} \
    --certificate-authority=ca.pem \
    --embed-certs=true \
    --server=https://${CLUSTER_INTERNAL_IP}:6443 \
    --kubeconfig=${instance}.kubeconfig

  kubectl config set-credentials system:node:${instance}.${DOMAIN_NAME} \
    --client-certificate=${instance}.pem \
    --client-key=${instance}-key.pem \
    --embed-certs=true \
    --kubeconfig=${instance}.kubeconfig

  kubectl config set-context default \
    --cluster=${DOMAIN_NAME} \
    --user=system:node:${instance}.${DOMAIN_NAME} \
    --kubeconfig=${instance}.kubeconfig

  kubectl config use-context default --kubeconfig=${instance}.kubeconfig
done

# The kube-proxy Kubernetes Configuration File
{
  kubectl config set-cluster ${DOMAIN_NAME} \
    --certificate-authority=ca.pem \
    --embed-certs=true \
    --server=https://${CLUSTER_INTERNAL_IP}:6443 \
    --kubeconfig=kube-proxy.kubeconfig

  kubectl config set-credentials system:kube-proxy \
    --client-certificate=kube-proxy.pem \
    --client-key=kube-proxy-key.pem \
    --embed-certs=true \
    --kubeconfig=kube-proxy.kubeconfig

  kubectl config set-context default \
    --cluster=${DOMAIN_NAME} \
    --user=system:kube-proxy \
    --kubeconfig=kube-proxy.kubeconfig

  kubectl config use-context default --kubeconfig=kube-proxy.kubeconfig
}

# The kube-controller-manager Kubernetes Configuration File
{
  kubectl config set-cluster ${DOMAIN_NAME} \
    --certificate-authority=ca.pem \
    --embed-certs=true \
    --server=https://127.0.0.1:6443 \
    --kubeconfig=kube-controller-manager.kubeconfig

  kubectl config set-credentials system:kube-controller-manager \
    --client-certificate=kube-controller-manager.pem \
    --client-key=kube-controller-manager-key.pem \
    --embed-certs=true \
    --kubeconfig=kube-controller-manager.kubeconfig

  kubectl config set-context default \
    --cluster=${DOMAIN_NAME} \
    --user=system:kube-controller-manager \
    --kubeconfig=kube-controller-manager.kubeconfig

  kubectl config use-context default --kubeconfig=kube-controller-manager.kubeconfig
}

# The kube-scheduler Kubernetes Configuration File
{
  kubectl config set-cluster ${DOMAIN_NAME} \
    --certificate-authority=ca.pem \
    --embed-certs=true \
    --server=https://127.0.0.1:6443 \
    --kubeconfig=kube-scheduler.kubeconfig

  kubectl config set-credentials system:kube-scheduler \
    --client-certificate=kube-scheduler.pem \
    --client-key=kube-scheduler-key.pem \
    --embed-certs=true \
    --kubeconfig=kube-scheduler.kubeconfig

  kubectl config set-context default \
    --cluster=${DOMAIN_NAME} \
    --user=system:kube-scheduler \
    --kubeconfig=kube-scheduler.kubeconfig

  kubectl config use-context default --kubeconfig=kube-scheduler.kubeconfig
}

# The admin Kubernetes Configuration File
{
  kubectl config set-cluster ${DOMAIN_NAME} \
    --certificate-authority=ca.pem \
    --embed-certs=true \
    --server=https://127.0.0.1:6443 \
    --kubeconfig=admin.kubeconfig

  kubectl config set-credentials admin \
    --client-certificate=admin.pem \
    --client-key=admin-key.pem \
    --embed-certs=true \
    --kubeconfig=admin.kubeconfig

  kubectl config set-context default \
    --cluster=${DOMAIN_NAME} \
    --user=admin \
    --kubeconfig=admin.kubeconfig

  kubectl config use-context default --kubeconfig=admin.kubeconfig
}

# Distribute the Kubernetes Configuration Files
cd ${CUR_DIR}

for instance in lb-infra-1 lb-infra-2 worker-1 worker-2 worker-3; do
  sshpass -f ../utils/password scp -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" ${K8S_WORK_DIR}/tls/${instance}.kubeconfig ${K8S_WORK_DIR}/tls/kube-proxy.kubeconfig root@${instance}.${DOMAIN_NAME}:~/
done

for instance in controller-1 controller-2 controller-3; do
  sshpass -f ../utils/password scp -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" ${K8S_WORK_DIR}/tls/admin.kubeconfig ${K8S_WORK_DIR}/tls/kube-controller-manager.kubeconfig ${K8S_WORK_DIR}/tls/kube-scheduler.kubeconfig root@${instance}.${DOMAIN_NAME}:~/
done