source ./.env

for instance in lb-infra-1 lb-infra-2 worker-1 worker-2 worker-3; do
  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${instance}.${DOMAIN_NAME} "echo '      routes:' >> ${PATH_NETPLAN_CONFIG}"
  
  NRO_SUB_NET=0
  
  for instance2 in lb-infra-1 lb-infra-2 worker-1 worker-2 worker-3; do
    if [ "$instance" != "$instance2" ]; then
      IP_INT=$(dig ${instance2}.int.${DOMAIN_NAME} +short)
      POD_CIDR_NEW=$(echo "${POD_CIDR/X/$NRO_SUB_NET}")
      
      sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${instance}.${DOMAIN_NAME} "echo '      - to: ${POD_CIDR_NEW}' >> ${PATH_NETPLAN_CONFIG}"
      sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${instance}.${DOMAIN_NAME} "echo '        via: ${IP_INT}' >> ${PATH_NETPLAN_CONFIG}"
    fi

    ((NRO_SUB_NET=NRO_SUB_NET+1))
  done

  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${instance}.${DOMAIN_NAME} "netplan apply"
done