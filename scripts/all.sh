sh create-dns-server.sh
# set ip dns to host
sh create-servers.sh
sh create-ca-tls-certificates.sh
sh create-config-auth.sh
sh create-encrypt-conf.sh
sh exec-boot-etcd.sh
sh exec-boot-control-plane.sh
sh exec-boot-workers-nodes.sh
sh config-remote-access.sh
sh add-routes-pods-workers-nodes.sh
sh exec-register-control-plane.sh # Agregar controladores a kubectl for get nodes
sh set-labels-taints.sh
sh install-addons.sh