source ./.env

for instance in controller-1 controller-2 controller-3; do
  sshpass -f ../utils/password scp -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" .env boot-control-plane.sh root@${instance}.${DOMAIN_NAME}:~/
  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${instance}.${DOMAIN_NAME} "sh boot-control-plane.sh"
  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${instance}.${DOMAIN_NAME} "rm boot-control-plane.sh"
done

sshpass -f ../utils/password scp -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" boot-control-plane-rbac.sh root@controller-1.${DOMAIN_NAME}:~/
sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@controller-1.${DOMAIN_NAME} "sh boot-control-plane-rbac.sh"
sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@controller-1.${DOMAIN_NAME} "rm .env boot-control-plane-rbac.sh"
