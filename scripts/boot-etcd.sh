#!/bin/bash

. ./.env

# Download and Install the etcd Binaries
wget -q --show-progress --https-only --timestamping \
  "https://github.com/etcd-io/etcd/releases/download/${ETCD_VERSION}/etcd-${ETCD_VERSION}-linux-amd64.tar.gz"

{
  tar -xvf etcd-${ETCD_VERSION}-linux-amd64.tar.gz
  mv etcd-${ETCD_VERSION}-linux-amd64/etcd* /usr/local/bin/
}

# Configure the etcd Server
{
  mkdir -p /etc/etcd /var/lib/etcd
  chmod 700 /var/lib/etcd
  cp ca.pem kubernetes-key.pem kubernetes.pem /etc/etcd/
}

{
INTERNAL_IP=$(ip a s enp0s8 | egrep -o 'inet [0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | cut -d' ' -f2)
#ETCD_NAME=$(hostname -s)
ETCD_NAME=$(hostname)
}

{
CONTROLLER_1_NAME=controller-1
CONTROLLER_1_INTERNAL_IP=$(dig ${CONTROLLER_1_NAME}.int.${DOMAIN_NAME} +short)
CONTROLLER_2_NAME=controller-2
CONTROLLER_2_INTERNAL_IP=$(dig ${CONTROLLER_2_NAME}.int.${DOMAIN_NAME} +short)
CONTROLLER_3_NAME=controller-3
CONTROLLER_3_INTERNAL_IP=$(dig ${CONTROLLER_3_NAME}.int.${DOMAIN_NAME} +short)

cat <<EOF | tee /etc/systemd/system/etcd.service
[Unit]
Description=etcd
Documentation=https://github.com/coreos

[Service]
Type=notify
ExecStart=/usr/local/bin/etcd \\
  --name ${ETCD_NAME} \\
  --cert-file=/etc/etcd/kubernetes.pem \\
  --key-file=/etc/etcd/kubernetes-key.pem \\
  --peer-cert-file=/etc/etcd/kubernetes.pem \\
  --peer-key-file=/etc/etcd/kubernetes-key.pem \\
  --trusted-ca-file=/etc/etcd/ca.pem \\
  --peer-trusted-ca-file=/etc/etcd/ca.pem \\
  --peer-client-cert-auth \\
  --client-cert-auth \\
  --initial-advertise-peer-urls https://${INTERNAL_IP}:2380 \\
  --listen-peer-urls https://${INTERNAL_IP}:2380 \\
  --listen-client-urls https://${INTERNAL_IP}:2379,https://127.0.0.1:2379 \\
  --advertise-client-urls https://${INTERNAL_IP}:2379 \\
  --initial-cluster-token etcd-cluster-0 \\
  --initial-cluster ${CONTROLLER_1_NAME}.${DOMAIN_NAME}=https://${CONTROLLER_1_INTERNAL_IP}:2380,${CONTROLLER_2_NAME}.${DOMAIN_NAME}=https://${CONTROLLER_2_INTERNAL_IP}:2380,${CONTROLLER_3_NAME}.${DOMAIN_NAME}=https://${CONTROLLER_3_INTERNAL_IP}:2380 \\
  --initial-cluster-state new \\
  --data-dir=/var/lib/etcd
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF
}

{
  systemctl daemon-reload
  systemctl enable etcd
  systemctl start etcd
}

ETCDCTL_API=3 etcdctl member list \
  --endpoints=https://127.0.0.1:2379 \
  --cacert=/etc/etcd/ca.pem \
  --cert=/etc/etcd/kubernetes.pem \
  --key=/etc/etcd/kubernetes-key.pem