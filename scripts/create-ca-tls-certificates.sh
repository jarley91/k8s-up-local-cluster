source ./.env

mkdir -p ${K8S_WORK_DIR}/tls
CUR_DIR=$(pwd)
cd ${K8S_WORK_DIR}/tls

# Certificate Authority
{

cat > ca-config.json <<EOF
{
  "signing": {
    "default": {
      "expiry": "${EXPIRE_HOURS}h"
    },
    "profiles": {
      "${CA_PROFILE}": {
        "usages": ["signing", "key encipherment", "server auth", "client auth"],
        "expiry": "${EXPIRE_HOURS}h"
      }
    }
  }
}
EOF

cat > ca-csr.json <<EOF
{
  "CN": "${CA_PROFILE}",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "${CA_U}",
      "L": "${CA_L}",
      "O": "${CA_PROFILE}",
      "OU": "CA",
      "ST": "${CA_ST}"
    }
  ]
}
EOF

cfssl gencert -initca ca-csr.json | cfssljson -bare ca

}

# The Admin Client Certificate
{

cat > admin-csr.json <<EOF
{
  "CN": "admin",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "${CA_U}",
      "L": "${CA_L}",
      "O": "system:masters",
      "OU": "${ADMIN_OU}",
      "ST": "${CA_ST}"
    }
  ]
}
EOF

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -profile=${CA_PROFILE} \
  admin-csr.json | cfssljson -bare admin

}

# The Kubelet Client Certificates
for instance in lb-infra-1 lb-infra-2 worker-1 worker-2 worker-3; do
cat > ${instance}-csr.json <<EOF
{
  "CN": "system:node:${instance}.${DOMAIN_NAME}",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "${CA_U}",
      "L": "${CA_L}",
      "O": "system:nodes",
      "OU": "${ADMIN_OU}",
      "ST": "${CA_ST}"
    }
  ]
}
EOF

IP_INTERNAL_INSTACE=$(dig ${instance}.int.${DOMAIN_NAME} +short)
IP_EXTERNAL_INSTACE=$(dig ${instance}.${DOMAIN_NAME} +short)

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -hostname=${instance},${instance}.${DOMAIN_NAME},${IP_INTERNAL_INSTACE},${IP_EXTERNAL_INSTACE} \
  -profile=${CA_PROFILE} \
  ${instance}-csr.json | cfssljson -bare ${instance}
done

# The Controller Manager Client Certificate
{

cat > kube-controller-manager-csr.json <<EOF
{
  "CN": "system:kube-controller-manager",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "${CA_U}",
      "L": "${CA_L}",
      "O": "system:kube-controller-manager",
      "OU": "${ADMIN_OU}",
      "ST": "${CA_ST}"
    }
  ]
}
EOF

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -profile=${CA_PROFILE} \
  kube-controller-manager-csr.json | cfssljson -bare kube-controller-manager

}

# The Kube Proxy Client Certificate
{

cat > kube-proxy-csr.json <<EOF
{
  "CN": "system:kube-proxy",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "${CA_U}",
      "L": "${CA_L}",
      "O": "system:node-proxier",
      "OU": "${ADMIN_OU}",
      "ST": "${CA_ST}"
    }
  ]
}
EOF

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -profile=${CA_PROFILE} \
  kube-proxy-csr.json | cfssljson -bare kube-proxy

}

# The Scheduler Client Certificate
{

cat > kube-scheduler-csr.json <<EOF
{
  "CN": "system:kube-scheduler",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "${CA_U}",
      "L": "${CA_L}",
      "O": "system:kube-scheduler",
      "OU": "${ADMIN_OU}",
      "ST": "${CA_ST}"
    }
  ]
}
EOF

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -profile=${CA_PROFILE} \
  kube-scheduler-csr.json | cfssljson -bare kube-scheduler

}

# The Kubernetes API Server Certificate
{

cat > kubernetes-csr.json <<EOF
{
  "CN": "${CA_PROFILE}",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "${CA_U}",
      "L": "${CA_L}",
      "O": "${CA_PROFILE}",
      "OU": "${ADMIN_OU}",
      "ST": "${CA_ST}"
    }
  ]
}
EOF

CONTROLLER_1_INTERNAL_IP=$(dig controller-1.int.${DOMAIN_NAME} +short)
CONTROLLER_2_INTERNAL_IP=$(dig controller-2.int.${DOMAIN_NAME} +short)
CONTROLLER_3_INTERNAL_IP=$(dig controller-3.int.${DOMAIN_NAME} +short)
CLUSTER_INTERNAL_IP=$(dig ${API_SERVER_INTENAL_NAME} +short)
CLUSTER_EXTERNAL_IP=$(dig ${API_SERVER_EXTERNAL_NAME} +short)

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -hostname=${CLUSTER_SERVICES_INET},${CONTROLLER_1_INTERNAL_IP},${CONTROLLER_2_INTERNAL_IP},${CONTROLLER_3_INTERNAL_IP},${CLUSTER_INTERNAL_IP},${CLUSTER_EXTERNAL_IP},127.0.0.1,${CLUSTER_HOSTNAMES} \
  -profile=${CA_PROFILE} \
  kubernetes-csr.json | cfssljson -bare kubernetes

}

# The Service Account Key Pair
{

cat > service-account-csr.json <<EOF
{
  "CN": "service-accounts",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "${CA_U}",
      "L": "${CA_L}",
      "O": "${CA_PROFILE}",
      "OU": "${ADMIN_OU}",
      "ST": "${CA_ST}"
    }
  ]
}
EOF

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -profile=${CA_PROFILE} \
  service-account-csr.json | cfssljson -bare service-account

}

# Distribute the Client and Server Certificates
cd ${CUR_DIR}

for instance in lb-infra-1 lb-infra-2 worker-1 worker-2 worker-3; do
  sshpass -f ../utils/password scp -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" ${K8S_WORK_DIR}/tls/ca.pem ${K8S_WORK_DIR}/tls/${instance}-key.pem ${K8S_WORK_DIR}/tls/${instance}.pem root@${instance}.${DOMAIN_NAME}:~/
done

for instance in controller-1 controller-2 controller-3; do
  sshpass -f ../utils/password scp -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" ${K8S_WORK_DIR}/tls/ca.pem ${K8S_WORK_DIR}/tls/ca-key.pem ${K8S_WORK_DIR}/tls/kubernetes-key.pem ${K8S_WORK_DIR}/tls/kubernetes.pem \
    ${K8S_WORK_DIR}/tls/service-account-key.pem ${K8S_WORK_DIR}/tls/service-account.pem root@${instance}.${DOMAIN_NAME}:~/
done

# Enable metrics
sh metrics-create-tls.sh