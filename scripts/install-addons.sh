# Coredns
helm repo add coredns https://coredns.github.io/helm
helm --namespace kube-system install coredns coredns/coredns --set service.clusterIP=10.201.0.10 --set replicaCount=2

# MetalLb - Con helm no asigna IP externa
#helm repo add metallb https://metallb.github.io/metallb
#helm install metallb metallb/metallb --namespace metallb-system --create-namespace
#kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.11.0/manifests/namespace.yaml
kubectl create namespace metallb-system
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.11.0/manifests/metallb.yaml
kubectl apply -f ../network/metallb-config-map.yaml

# Metrics Server
helm repo add metrics-server https://kubernetes-sigs.github.io/metrics-server/
helm install metrics-server metrics-server/metrics-server --namespace metrics-server --create-namespace
helm repo add kubernetes-dashboard https://kubernetes.github.io/dashboard/
helm install kubernetes-dashboard kubernetes-dashboard/kubernetes-dashboard --namespace kubernetes-dashboard --create-namespace
kubectl apply -f ../addons/kubernetes-dashboard/admin-user-sa.yaml
kubectl apply -f ../addons/kubernetes-dashboard/admin-user-crb.yaml
#kubectl -n kubernetes-dashboard get secret $(kubectl -n kubernetes-dashboard get sa/admin-user -o jsonpath="{.secrets[0].name}") -o go-template="{{.data.token | base64decode}}"

# Ingress
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
#helm show values ingress-nginx/ingress-nginx > /tmp/ingress-nginx.yaml
# Change hostNetwork: true | hostPort: enabled: true | kind: Deployment -> kind: DaemonSet
helm install ingress-nginx ingress-nginx/ingress-nginx --namespace ingress-nginx --create-namespace \
  --set controller.metrics.enabled=true \
  --set-string controller.podAnnotations."prometheus\.io/scrape"="true" \
  --set-string controller.podAnnotations."prometheus\.io/port"="10254" #\
  #--values /tmp/ingress-nginx.yaml

# Prometheus & Grafana
kubectl -n ingress-nginx apply --kustomize github.com/kubernetes/ingress-nginx/deploy/prometheus/
kubectl -n ingress-nginx apply --kustomize github.com/kubernetes/ingress-nginx/deploy/grafana/

# Create ingress
kubectl apply -f ../yamls/ingress/ingress-kubernetes-dashboard.yaml
kubectl apply -f ../yamls/ingress/ingress-grafana.yaml