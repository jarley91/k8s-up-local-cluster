source ./.env

NRO_SUB_NET=0

for instance in lb-infra-1 lb-infra-2 worker-1 worker-2 worker-3; do
  sshpass -f ../utils/password scp -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" .env boot-workers-nodes.sh root@${instance}.${DOMAIN_NAME}:~/
  POD_CIDR_NEW=$(echo "${POD_CIDR/X/$NRO_SUB_NET}")
  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${instance}.${DOMAIN_NAME} "sed -i 's/_POD_CIDR_/${POD_CIDR_NEW//\//\\/}/g' boot-workers-nodes.sh"
  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${instance}.${DOMAIN_NAME} "sh boot-workers-nodes.sh"
  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${instance}.${DOMAIN_NAME} "rm .env boot-workers-nodes.sh"

  ((NRO_SUB_NET=NRO_SUB_NET+1))
done