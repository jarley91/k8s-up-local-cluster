source ./.env

sh register-control-plane-tls.sh

NRO_SUB_NET=5

for instance in controller-1 controller-2 controller-3; do
  sshpass -f ../utils/password scp -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" .env register-control-plane.sh root@${instance}.${DOMAIN_NAME}:~/
  POD_CIDR_NEW=$(echo "${POD_CIDR/X/$NRO_SUB_NET}")
  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${instance}.${DOMAIN_NAME} "sed -i 's/_POD_CIDR_/${POD_CIDR_NEW//\//\\/}/g' register-control-plane.sh"
  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${instance}.${DOMAIN_NAME} "sh register-control-plane.sh"
  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${instance}.${DOMAIN_NAME} "rm .env register-control-plane.sh"

  ((NRO_SUB_NET=NRO_SUB_NET+1))
done

# Add routes
for instance in controller-1 controller-2 controller-3; do
  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${instance}.${DOMAIN_NAME} "echo '      routes:' >> ${PATH_NETPLAN_CONFIG}"
  
  NRO_SUB_NET=0

  for instance2 in lb-infra-1 lb-infra-2 worker-1 worker-2 worker-3 controller-1 controller-2 controller-3; do
    if [ "$instance" != "$instance2" ]; then
      IP_INT=$(dig ${instance2}.int.${DOMAIN_NAME} +short)
      POD_CIDR_NEW=$(echo "${POD_CIDR/X/$NRO_SUB_NET}")
      
      sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${instance}.${DOMAIN_NAME} "echo '      - to: ${POD_CIDR_NEW}' >> ${PATH_NETPLAN_CONFIG}"
      sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${instance}.${DOMAIN_NAME} "echo '        via: ${IP_INT}' >> ${PATH_NETPLAN_CONFIG}"
    fi

    ((NRO_SUB_NET=NRO_SUB_NET+1))
  done

  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${instance}.${DOMAIN_NAME} "netplan apply"
done

for instance in lb-infra-1 lb-infra-2 worker-1 worker-2 worker-3; do  
  NRO_SUB_NET=5
  
  for instance2 in controller-1 controller-2 controller-3; do
    if [ "$instance" != "$instance2" ]; then
      IP_INT=$(dig ${instance2}.int.${DOMAIN_NAME} +short)
      POD_CIDR_NEW=$(echo "${POD_CIDR/X/$NRO_SUB_NET}")
      
      sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${instance}.${DOMAIN_NAME} "echo '      - to: ${POD_CIDR_NEW}' >> ${PATH_NETPLAN_CONFIG}"
      sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${instance}.${DOMAIN_NAME} "echo '        via: ${IP_INT}' >> ${PATH_NETPLAN_CONFIG}"
    fi

    ((NRO_SUB_NET=NRO_SUB_NET+1))
  done

  sshpass -f ../utils/password ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" root@${instance}.${DOMAIN_NAME} "netplan apply"
done