source ./.env

CUR_DIR=$(pwd)
cd ${K8S_WORK_DIR}/tls

# The Kubelet Client Certificates
for instance in controller-1 controller-2 controller-3; do
cat > ${instance}-csr.json <<EOF
{
  "CN": "system:node:${instance}.${DOMAIN_NAME}",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "${CA_U}",
      "L": "${CA_L}",
      "O": "system:nodes",
      "OU": "${ADMIN_OU}",
      "ST": "${CA_ST}"
    }
  ]
}
EOF

IP_INTERNAL_INSTACE=$(dig ${instance}.int.${DOMAIN_NAME} +short)
IP_EXTERNAL_INSTACE=$(dig ${instance}.${DOMAIN_NAME} +short)

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -hostname=${instance},${instance}.${DOMAIN_NAME},${IP_INTERNAL_INSTACE},${IP_EXTERNAL_INSTACE} \
  -profile=${CA_PROFILE} \
  ${instance}-csr.json | cfssljson -bare ${instance}
done

# The kubelet Kubernetes Configuration File
CLUSTER_INTERNAL_IP=$(dig ${API_SERVER_INTENAL_NAME} +short)

for instance in controller-1 controller-2 controller-3; do
  kubectl config set-cluster ${DOMAIN_NAME} \
    --certificate-authority=ca.pem \
    --embed-certs=true \
    --server=https://${CLUSTER_INTERNAL_IP}:6443 \
    --kubeconfig=${instance}.kubeconfig

  kubectl config set-credentials system:node:${instance}.${DOMAIN_NAME} \
    --client-certificate=${instance}.pem \
    --client-key=${instance}-key.pem \
    --embed-certs=true \
    --kubeconfig=${instance}.kubeconfig

  kubectl config set-context default \
    --cluster=${DOMAIN_NAME} \
    --user=system:node:${instance}.${DOMAIN_NAME} \
    --kubeconfig=${instance}.kubeconfig

  kubectl config use-context default --kubeconfig=${instance}.kubeconfig
done

# Distribute the Client and Server Certificates
cd ${CUR_DIR}

for instance in controller-1 controller-2 controller-3; do
  sshpass -f ../utils/password scp -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" ${K8S_WORK_DIR}/tls/ca.pem ${K8S_WORK_DIR}/tls/${instance}-key.pem ${K8S_WORK_DIR}/tls/${instance}.pem ${K8S_WORK_DIR}/tls/${instance}.kubeconfig ${K8S_WORK_DIR}/tls/kube-proxy.kubeconfig root@${instance}.${DOMAIN_NAME}:~/
done