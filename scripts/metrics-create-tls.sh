source ./.env

CUR_DIR=$(pwd)
cd ${K8S_WORK_DIR}/tls

cfssl gencert -initca ca-csr.json | cfssljson -bare front-proxy-ca

cat > front-proxy-csr.json <<EOF
{
  "CN": "front-proxy-ca",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "${CA_U}",
      "L": "${CA_L}",
      "O": "${CA_PROFILE}",
      "OU": "CA",
      "ST": "${CA_ST}"
    }
  ]
}
EOF

cfssl gencert \
   -ca=front-proxy-ca.pem \
   -ca-key=front-proxy-ca-key.pem \
   -config=ca-config.json \
   -profile=${CA_PROFILE} \
   front-proxy-csr.json | cfssljson -bare front-proxy

cd ${CUR_DIR}

for instance in controller-1 controller-2 controller-3; do
  sshpass -f ../utils/password scp -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" ${K8S_WORK_DIR}/tls/front-proxy-ca.pem ${K8S_WORK_DIR}/tls/front-proxy.pem ${K8S_WORK_DIR}/tls/front-proxy-key.pem root@${instance}.${DOMAIN_NAME}:~/
done