source ./.env

for instance in controller-1 controller-2 controller-3; do
  kubectl label node ${instance}.${DOMAIN_NAME} node-role.kubernetes.io/master=master
  kubectl label node ${instance}.${DOMAIN_NAME} node-role.kubernetes.io/control-plane=control-plane
  kubectl taint node ${instance}.${DOMAIN_NAME} node.kubernetes.io=unschedulable:NoSchedule
done

for instance in lb-infra-1 lb-infra-2; do
  kubectl label node ${instance}.${DOMAIN_NAME} node-role.kubernetes.io/infra=infra
done

for instance in worker-1 worker-2 worker-3; do
  kubectl label node ${instance}.${DOMAIN_NAME} node-role.kubernetes.io/worker=worker
done